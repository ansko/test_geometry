cmake_minimum_required(VERSION 3.10)


project (test_oop_geometry) 


find_package(Qt5 COMPONENTS Widgets REQUIRED)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED on)


if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()


set (HEADERS
    src/dummy_widget.hpp
    src/base_geometry.hpp
     src/point.hpp
     src/base_figure.hpp
      src/polygon.hpp
       src/triangle.hpp
       src/rectangle.hpp
        src/square.hpp
      src/circle.hpp
)


add_executable (main ${HEADERS} src/main.cpp)
target_link_libraries(main Qt5::Widgets)
