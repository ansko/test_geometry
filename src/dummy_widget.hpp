#ifndef SRC_DUMMY_WIDGET_HPP
#define SRC_DUMMY_WIDGET_HPP


#include <memory>
#include <QWidget>


namespace testns
{


class DummyWidget : public QWidget
{
public:
    explicit DummyWidget(QWidget *parent=0)
    {
        this->resize(1000, 500);
    }

    void add_figure(const BaseGeometry& figure)
    {
        _ptrs.emplace_back(figure.get_new_ptr());
    }

    void paintEvent(QPaintEvent *event)
    {
        QPainter p(this);
        for (auto &ptr : _ptrs)
            ptr->paint(&p);
    }
private:
    std::vector<std::unique_ptr<BaseGeometry>> _ptrs;
};


} // ns testns


#endif // #ifndef SRC_DUMMY_WIDGET_HPP
