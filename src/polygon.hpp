#ifndef SRC_POLYGON_HPP
#define SRC_POLYGON_HPP


#include <cmath>

#include "base_figure.hpp"


extern const float EPS;


namespace testns
{


class Polygon : public BaseFigure
{
public:
    Polygon(const std::vector<Point> &vertices, const QColor pen_color=Qt::black, const float pen_width=1)
    : BaseFigure(pen_color, pen_width),
      _vertices(vertices)
    {
        // Check that 3 neighbring vertices do not lie on a single line
        for (size_t idx = 0; idx < _vertices.size(); ++idx)
          {
            size_t idx_min, idx_max;
            if (idx > 0)
                idx_min = idx - 1;
            else
                idx_min = _vertices.size() - 1;
            if (idx < _vertices.size() - 1)
                idx_max = idx + 1;
            else
                idx_max = 0;

            const float x1 = _vertices[idx_min].x();
            const float x2 = _vertices[idx].x();
            const float x3 = _vertices[idx_max].x();
            const float y1 = _vertices[idx_min].y();
            const float y2 = _vertices[idx].y();
            const float y3 = _vertices[idx_max].y();
            const float det = std::fabs((x1 - x2) * (y1 - y3) - (x1 - x3) * (y1 - y2));
            const float dr = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

            if (dr < EPS) // neighboring vertices are too close
                throw;
            if (det < EPS * dr) // 3 neighboring vertices seem to lie on a single line
                throw;
          }
    }

    void paint(QPainter *painter) const override
    {
        _init_painter(painter);

        for (size_t idx = 0; idx < _vertices.size(); ++idx)
          {
            QPointF pt1 = QPointF(_vertices[idx].x(), _vertices[idx].y()), pt2;
            if (idx > 0)
                pt2 = QPointF(_vertices[idx - 1].x(), _vertices[idx - 1].y());
            else
                pt2 = QPointF(_vertices[_vertices.size() - 1].x(), _vertices[_vertices.size() - 1].y());
            painter->drawLine(pt1, pt2);
          }
    }

    const Point center() const override
    {
        float x = 0, y = 0;
        for (auto &v : _vertices)
          {
            x += v.x();
            y += v.y();
          }
        return Point(x / _vertices.size(), y / _vertices.size());
    }

    void rescale(const float factor) override
    {
        float cx = center().x(), cy = center().y();
        for (size_t idx = 0; idx < _vertices.size(); ++idx)
         {
            float dx = (_vertices[idx].x() - cx) * (factor - 1);
            float dy = (_vertices[idx].y() - cy) * (factor - 1);
            _vertices[idx].translate(dx, dy);
          }
    }

    void translate(const float dx, const float dy) override
    {
        for (auto &v : _vertices)
          v.translate(dx, dy);
    }

    void rotate(const float angle) override
    {
        for (size_t idx = 0; idx < _vertices.size(); ++idx)
          {
            const float x = _vertices[idx].x() * cos(angle) - _vertices[idx].y() * sin(angle);
            const float y = _vertices[idx].x() * sin(angle) + _vertices[idx].y() * cos(angle);
            _vertices[idx] = Point(x, y);
          }
    }

    Polygon* get_new_ptr() const override final
    {
        return new Polygon(*this);
    }
protected:
    Polygon();
    std::vector<Point> _vertices;
};


} // ns testns


#endif // #ifndef SRC_POLYGON_HPP
