#include <QApplication>


#include "point.hpp"
#include "polygon.hpp"
#include "circle.hpp"
#include "dummy_widget.hpp"
#include "triangle.hpp"
#include "rectangle.hpp"
#include "square.hpp"


const float EPS = 1e-6;


int main(int argc, char **argv)
{
    QApplication my_app(argc, argv);

    testns::DummyWidget my_label;


    // Polygon (shaped as rectange)
    {
        testns::Point A(50, 50), B(150, 50), C(150, 150), D(50, 150);
        testns::Polygon poly({A, B, C, D}, Qt::green, 5);
        poly.rotate(-0.25);
        my_label.add_figure(poly);
    }

    // Circle
    {
        testns::Circle cir(testns::Point(300, 300), 100, Qt::blue, 15);
        my_label.add_figure(cir);
    }

    // Point
    {
        testns::Point pt(400, 100, Qt::red, 10);
        my_label.add_figure(pt);
    }

    // Triangle
    {
        testns::Point A(500, 50), B(650, 50), C(650, 150);
        testns::Triangle tri({A, B, C}, Qt::yellow, 5);
        my_label.add_figure(tri);
    }

    // Rectangles
    {
        testns::Point A(500, 250), B(650, 250), C(650, 350), D(500, 350);
        testns::Rectangle rec1({A, B, C, D}, Qt::cyan, 5);
        my_label.add_figure(rec1);

        A.translate(200, 50);
        C.translate(200, 50);
        testns::Rectangle rec2(A, C, Qt::magenta, 5);
        rec2.rotate(0.1);
        my_label.add_figure(rec2);
    }

    // Square
    {
        testns::Point A(700, 50), B(850, 50), C(850, 200), D(700, 200);
        testns::Square sq1({A, B, C, D}, Qt::black, 5);
        my_label.add_figure(sq1);

        testns::Square sq2(C, 150, Qt::red, 5);
        sq2.rescale(0.5);
        my_label.add_figure(sq2);
    }

    my_label.show();

    return my_app.exec();
}
