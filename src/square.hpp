#ifndef SRC_SQUARE_HPP
#define SRC_SQUARE_HPP


extern const float EPS;


namespace testns
{


class Square : public Rectangle
{
public:
    Square(const Point &pt_min, const float edge_length, const QColor pen_color=Qt::black, const float pen_width=1)
    : Square(pt_min, Point(pt_min.x() + edge_length, pt_min.y() + edge_length), pen_color, pen_width)
    {}

    Square(const Point &pt_min, const Point &pt_max, const QColor pen_color=Qt::black, const float pen_width=1)
    : Rectangle(pt_min, pt_max, pen_color, pen_width)
    {
      if (std::fabs(std::fabs(pt_max.x() - pt_min.x()) - std::fabs(pt_max.y() - pt_min.y())) > EPS)
          throw;
    }

    Square(const std::vector<Point> &vertices, const QColor pen_color=Qt::black, const float pen_width=1)
    : Rectangle(vertices, pen_color, pen_width)
    {
        for (size_t idx = 0; idx < _vertices.size(); ++idx)
          {
            size_t idx_min, idx_max;
            if (idx > 0)
                idx_min = idx - 1;
            else
                idx_min = _vertices.size() - 1;
            if (idx < _vertices.size() - 1)
                idx_max = idx + 1;
            else
                idx_max = 0;

            const float dx_max2 = pow(_vertices[idx_max].x() - _vertices[idx].x(), 2);
            const float dx_min2 = pow(_vertices[idx_min].x() - _vertices[idx].x(), 2);
            const float dy_max2 = pow(_vertices[idx_max].y() - _vertices[idx].y(), 2);
            const float dy_min2 = pow(_vertices[idx_min].y() - _vertices[idx].y(), 2);
            if (std::fabs(dx_max2 + dy_max2 - dx_min2 - dy_min2) > EPS) // neighboring edges are not of equal length
                throw;
          }
    }
};


} // ns testns


#endif // #ifndef SRC_SQUARE_HPP
