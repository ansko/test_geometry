#ifndef SRC_POINT_HPP
#define SRC_POINT_HPP


#include "base_geometry.hpp"


namespace testns
{


class Point : public BaseGeometry
{
public:
    Point(const float x, const float y, const QColor pen_color=Qt::black, const float pen_width=1)
    : BaseGeometry(pen_color, pen_width),
      _x(x),
      _y(y)
    {}

    const float x() const { return _x; }
    const float y() const { return _y; }

    virtual void paint(QPainter *painter) const override final
    {
        _init_painter(painter);

        painter->drawPoint(_x, _y);
        return;
    }

    virtual void translate(const float dx, const float dy) override final
    {
        _x += dx;
        _y += dy;
    }

    virtual Point* get_new_ptr() const override final
    {
        return new Point(*this);
    }

    Point &operator= (Point const &pt)
    {
       if (this != &pt)
         {
           _x = pt.x();
           _y = pt.y();
           _pen_color = pt.pen_color();
           _pen_width = pt.pen_width();
         }
       return *this;
    }

private:
    float _x, _y;
};


}  // ns testns


#endif // #ifndef SRC_POINT_HPP
