#ifndef SRC_RECTANGLE_HPP
#define SRC_RECTANGLE_HPP


extern const float EPS;


namespace testns
{


class Rectangle : public Polygon
{
public:
    Rectangle(const Point &pt_min, const Point &pt_max, const QColor pen_color=Qt::black, const float pen_width=1)
    : Rectangle({pt_min, Point(pt_min.x(),pt_max.y()), pt_max, Point(pt_max.x(), pt_min.y())}, pen_color, pen_width)
    {}

    Rectangle(const std::vector<Point> &vertices, const QColor pen_color=Qt::black, const float pen_width=1)
    : Polygon(vertices, pen_color, pen_width)
    {
        if (vertices.size() != 4)
            throw;

        for (size_t idx = 0; idx < _vertices.size(); ++idx)
          {
            size_t idx_min, idx_max;
            if (idx > 0)
                idx_min = idx - 1;
            else
                idx_min = _vertices.size() - 1;
            if (idx < _vertices.size() - 1)
                idx_max = idx + 1;
            else
                idx_max = 0;

            const float dx_max = _vertices[idx_max].x() - _vertices[idx].x();
            const float dx_min = _vertices[idx_min].x() - _vertices[idx].x();
            const float dy_max = _vertices[idx_max].y() - _vertices[idx].y();
            const float dy_min = _vertices[idx_min].y() - _vertices[idx].y();
            if (dx_max * dx_min + dy_max * dy_min > EPS) // neighboring edges are not perpendicular
                throw;
          }
    }
};


} // ns testns


#endif // #ifndef SRC_RECTANGLE_HPP
