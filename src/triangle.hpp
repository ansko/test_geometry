#ifndef SRC_TRIANGLE_HPP
#define SRC_TRIANGLE_HPP


namespace testns
{


class Triangle : public Polygon
{
public:
    Triangle(const std::vector<Point> &vertices, const QColor pen_color=Qt::black, const float pen_width=1)
    : Polygon(vertices, pen_color, pen_width)
    {
        if (vertices.size() != 3)
            throw;
    }
};


}  // ns testns



#endif // #ifndef SRC_TRIANGLE_HPP
