#ifndef SRC_CIRCLE_HPP
#define SRC_CIRCLE_HPP


#include "base_geometry.hpp"


namespace testns
{


class Circle : public BaseFigure
{
public:
    Circle(const Point center, const float r, const QColor pen_color=Qt::black, const float pen_width=1)
    : BaseFigure(pen_color, pen_width),
      _center(center),
      _r(r)
    {}

    void paint(QPainter * const painter) const override final
    {
        _init_painter(painter);
        painter->drawEllipse(QPointF(_center.x(), _center.y()), _r, _r);
    }

    const Point center() const override final { return _center; }
    const double r() const { return _r; }

    void rescale(float factor) override final
    {
        _r *= factor;
    }

    void translate(const float dx, const float dy) override final
    {
        _center.translate(dx, dy);
    }

    void rotate(const float angle) override final { return; }

    Circle* get_new_ptr() const override final
    {
        return new Circle(*this);
    }

private:
    Point _center;
    double _r;
};


} // ns testns


#endif // #ifndef SRC_CIRCLE_HPP
