#ifndef BASE_FIGURE_HPP
#define BASE_FIGURE_HPP


#include "base_geometry.hpp"


namespace testns
{


class BaseFigure : public BaseGeometry
{
public:
    BaseFigure(const QColor pen_color=Qt::black, const float pen_width=1)
    : BaseGeometry(pen_color, pen_width)
    {}

    // Change the size of the figure by changing distances between its center and outer parts
    virtual void rescale(float factor) = 0;

    // Get geometrical center of the figure
    virtual const Point center() const = 0;

    virtual void rotate(const float angle) = 0;

};


} // ns testns


#endif // #ifndef BASE_FIGURE_HPP
