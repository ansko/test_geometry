#ifndef SRC_BASE_GEOMETRY_HPP
#define SRC_BASE_GEOMETRY_HPP


#include <QPainter>


namespace testns
{


class BaseGeometry
{
public:
    BaseGeometry(const QColor pen_color=Qt::black, const float pen_width=1)
    : _pen_color(pen_color),
      _pen_width(pen_width)
    {}

    // Paint self on widget
    virtual void paint(QPainter * const painter) const = 0;

    // Move (everything can be translated, but not everything can be rotated)
    virtual void translate(const float dx, const float dy) = 0;

    virtual BaseGeometry* get_new_ptr() const = 0;

    const QColor pen_color() const { return _pen_color; }
    const float pen_width() const { return _pen_width; }
protected:
    void _init_painter(QPainter *painter) const
    {
        QPen pen = QPen(_pen_color);
        pen.setWidth(_pen_width);
        painter->setPen(pen);
    }

    QColor _pen_color;
    float _pen_width;
};


} // ns testns


#endif // #ifndef SRC_BASE_GEOMETRY_HPP
